#include "WS2812_Control.h"

#include "GrinLight.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "ws2812.h"

#define LOG_LOCAL_LEVEL ESP_LOG_INFO
#include "esp_log.h"

#define WS2812_QUEUE_TICKS_RECEIVE_WAIT					(1000)
#define WS2812_QUEUE_TICKS_SEND_WAIT					(1000)
#define WS2812_SET_COLOR_WITH_DURATION_STEP_COUNT		(20)
#define WS2812_SET_COLOR_WITH_DURATION_STEP_DELAY_MS	(35)

typedef enum {
	NONE_MODE = 0x00,
	STATIC_COLOR_MODE,
	PLAY_ANIM_MODE,
	PLAY_SOUND_ANIM_MODE
} WS2812_Mode_t;

typedef struct {
	WS2812_Mode_t Mode;
	WS2812_Color_t Color;
} WS2812_Queue_Item_t;

static const char* TAG = "[WS2812_Control]";

static TaskHandle_t WS2812_Task_Handle = NULL;
static QueueHandle_t WS2812_Queue_Handle = NULL;

static ws2812_t ws2812_handle = {
	.gpio_pin = WS2812_GPIO_PIN,
	.leds_count = WS2812_LEDS_COUNT,
	.leds_type = WS2812_LEDS_TYPE,
	.brightness = 100
};

static void WS2812_Task();
static void Set_Color_With_Duration(WS2812_Color_t New_Color, WS2812_Color_t Old_Color);

void WS2812_SetColor(void *arg) {

    if(arg == NULL)
        return;

	WS2812_Color_t *color = (WS2812_Color_t*)arg;
	WS2812_Queue_Item_t Item = {
		.Mode = STATIC_COLOR_MODE,
		.Color = {
			.Red = color->Red,
			.Green = color->Green,
			.Blue = color->Blue
		}
	};
	xQueueSend(WS2812_Queue_Handle, (void*)&Item, WS2812_QUEUE_TICKS_SEND_WAIT);
    ESP_LOGI(TAG, "Set color: %d %d %d !!!", color->Red, color->Green, color->Blue);
}

void WS2812_PowerOn(void* arg) {
	WS2812_Queue_Item_t Item = {
		.Mode = STATIC_COLOR_MODE,
		.Color = {
			.Red = 0xFF,
			.Green = 0xFF,
			.Blue = 0xFF
		}
	};
	xQueueSend(WS2812_Queue_Handle, (void*)&Item, WS2812_QUEUE_TICKS_SEND_WAIT);
	ESP_LOGI(TAG, "LED ON");
}

void WS2812_PowerOff(void* arg) {
	WS2812_Queue_Item_t Item = {
			.Mode = STATIC_COLOR_MODE,
			.Color = {
					.Red = 0x00,
					.Green = 0x00,
					.Blue = 0x00
			}
	};
	xQueueSend(WS2812_Queue_Handle, (void*)&Item, WS2812_QUEUE_TICKS_SEND_WAIT);
	ESP_LOGI(TAG, "LED OFF");
}

esp_err_t WS2812_init() {

	esp_err_t ret;
	ret = ws2812_init(ws2812_handle);

	xTaskCreate(WS2812_Task,
				"WS2812_Task",
				WS2812_Task_Depth,
				NULL,
				WS2812_Task_Priority,
				&WS2812_Task_Handle);

	WS2812_Queue_Handle = xQueueCreate( 3, sizeof(WS2812_Queue_Item_t) );

	return ret;
}

esp_err_t WS2812_denit() {

	if(WS2812_Task_Handle == NULL)
		return ESP_FAIL;

	ws2812_denit(ws2812_handle);
	vTaskDelete(WS2812_Task_Handle);
	WS2812_Task_Handle = NULL;

	return ESP_OK;
}

static void WS2812_Task() {

	WS2812_Queue_Item_t WS2812_Queue_Item;
	WS2812_Color_t Old_Color = {0};
	for(;;) {
		if( xQueueReceive( WS2812_Queue_Handle, &WS2812_Queue_Item,
				WS2812_QUEUE_TICKS_RECEIVE_WAIT / portTICK_PERIOD_MS ) == pdTRUE) {
			switch(WS2812_Queue_Item.Mode) {
				case STATIC_COLOR_MODE:
					Set_Color_With_Duration(WS2812_Queue_Item.Color, Old_Color);
					Old_Color = WS2812_Queue_Item.Color;
					break;
				default:
					break;
			}
		}
	}

	vTaskDelete(NULL);
}

static void Set_Color_With_Duration(WS2812_Color_t New_Color, WS2812_Color_t Old_Color) {

	WS2812_Color_t Color_Diff;
	Color_Diff.Red = abs( Old_Color.Red - New_Color.Red );
	Color_Diff.Green = abs( Old_Color.Green - New_Color.Green );
	Color_Diff.Blue = abs(Old_Color.Blue - New_Color.Blue );

	Color_Diff.Red /= WS2812_SET_COLOR_WITH_DURATION_STEP_COUNT;
	Color_Diff.Green /= WS2812_SET_COLOR_WITH_DURATION_STEP_COUNT;
	Color_Diff.Blue /= WS2812_SET_COLOR_WITH_DURATION_STEP_COUNT;

	for(int i = 0; i < WS2812_SET_COLOR_WITH_DURATION_STEP_COUNT; i++) {

		if(Old_Color.Red < New_Color.Red)
			Old_Color.Red += Color_Diff.Red;
		else
			Old_Color.Red -= Color_Diff.Red;

		if(Old_Color.Green < New_Color.Green)
			Old_Color.Green += Color_Diff.Green;
		else
			Old_Color.Green -= Color_Diff.Green;

		if(Old_Color.Blue < New_Color.Blue)
			Old_Color.Blue += Color_Diff.Blue;
		else
			Old_Color.Blue -= Color_Diff.Blue;

		for (int led = 0; led < WS2812_LEDS_COUNT; led++){
			ws2812_setColor(ws2812_handle, led, Old_Color.Red, Old_Color.Green, Old_Color.Blue);
		}
		ws2812_show(ws2812_handle);
		vTaskDelay(WS2812_SET_COLOR_WITH_DURATION_STEP_DELAY_MS / portTICK_RATE_MS);
	}

	for (int led = 0; led < WS2812_LEDS_COUNT; led++){
		ws2812_setColor(ws2812_handle, led, New_Color.Red, New_Color.Green, New_Color.Blue);
	}
	ws2812_show(ws2812_handle);
}
