#ifndef WS2812_CONTROL_H
#define WS2812_CONTROL_H

#include "esp_err.h"

#define WS2812_Task_Depth			(5*1024)
#define WS2812_Task_Priority		(5)

typedef struct {
	uint8_t Red;
	uint8_t Green;
	uint8_t Blue;
} WS2812_Color_t;

void WS2812_PowerOn();
void WS2812_PowerOff();
void WS2812_SetColor(void *arg);

esp_err_t WS2812_init();
esp_err_t WS2812_denit();

#endif
