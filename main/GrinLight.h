#ifndef GRINLIGHT_H
#define GRINLIGHT_H

#define MAIN_CYCLE_DELAY_MS				(1*1000)

#define WS2812_GPIO_PIN					(12)
#define WS2812_LEDS_COUNT				(30)
#define WS2812_LEDS_TYPE				(1)

#define DEFAULT_STA_SSID				"GrinLink Wi-fi"
#define DEFAULT_STA_PASSWORD			"Olga2512Grin"

#define WIFI_AP_SSID					"GrinLight"
#define WIFI_RECONN_PERIOD_MS			(60*1000)

#define MQTT_URI						("mqtt://192.168.0.125")
#define MQTT_PORT						(1883)
#define MQTT_USERNAME					("Agrin")
#define MQTT_PASSWORD					("Ggrin")
#define MQTT_TASK_PRIO 					(3)

#endif//GRINLIGHT_H
