#include "mqttModule.h"

#include "string.h"
#include "esp_err.h"

#define LOG_LOCAL_LEVEL ESP_LOG_DEBUG
#include "esp_log.h"

static char* TAG = "[mqttModule]";

static int mqttCommand_count = 0;
static mqtt_command_t* mqtt_command_list = NULL;

static esp_mqtt_client_handle_t mqtt_client = NULL;

static esp_err_t mqtt_event_handler(esp_mqtt_event_handle_t event);
static void mqttModule_cmd_handler(esp_mqtt_event_handle_t event);
static void mqttModule_cmd_subscribe(esp_mqtt_event_handle_t event);

void mqttCommand_Register(mqtt_command_t mqtt_command) {

	mqtt_command_list = realloc(mqtt_command_list, sizeof(mqtt_command_t)*(mqttCommand_count+1) );

	mqtt_command_list[mqttCommand_count].topic = mqtt_command.topic;
	mqtt_command_list[mqttCommand_count].data = mqtt_command.data;
	mqtt_command_list[mqttCommand_count].call_back = mqtt_command.call_back;

	mqttCommand_count++;
}

esp_err_t mqttModule_Start(esp_mqtt_client_config_t mqtt_config) {

	if(mqtt_client)
		return ESP_FAIL;

	esp_err_t ret;
	const esp_mqtt_client_config_t mqttModule_config = {
			.event_handle = mqtt_event_handler,
			.uri = mqtt_config.uri,
			.port = mqtt_config.port,
			.username = mqtt_config.username,
			.password = mqtt_config.password,
			.task_prio = mqtt_config.task_prio
	};

	mqtt_client = esp_mqtt_client_init(&mqttModule_config);
	ret = esp_mqtt_client_start(mqtt_client);

	return ret;
}

esp_err_t mqttModule_Stop() {

	esp_err_t ret;

	if(mqtt_client) {

		ret = esp_mqtt_client_stop(mqtt_client);
		if(ret != ESP_OK)
				return ret;

		ret = esp_mqtt_client_destroy(mqtt_client);
		if(ret != ESP_OK)
				return ret;

		mqtt_client = NULL;
	}

	if(mqtt_command_list) {
		free(mqtt_command_list);
		mqtt_command_list = NULL;
	}

	return ESP_OK;
}

static esp_err_t mqtt_event_handler(esp_mqtt_event_handle_t event) {

    switch (event->event_id) {
        case MQTT_EVENT_CONNECTED:
            ESP_LOGD(TAG, "MQTT_EVENT_CONNECTED");
            mqttModule_cmd_subscribe(event);
            break;
        case MQTT_EVENT_DISCONNECTED:
            ESP_LOGD(TAG, "MQTT_EVENT_DISCONNECTED");
            break;
        case MQTT_EVENT_SUBSCRIBED:
            ESP_LOGD(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_UNSUBSCRIBED:
            ESP_LOGD(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_PUBLISHED:
            ESP_LOGD(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_DATA:
            ESP_LOGD(TAG, "MQTT_EVENT_DATA");
            ESP_LOGD(TAG,"TOPIC=%.*s", event->topic_len, event->topic);
            ESP_LOGD(TAG,"DATA=%.*s", event->data_len, event->data);
            mqttModule_cmd_handler(event);
            break;
        case MQTT_EVENT_ERROR:
            ESP_LOGD(TAG, "MQTT_EVENT_ERROR");
            break;
        default:
            ESP_LOGD(TAG, "Other event id:%d", event->event_id);
            break;
    }
    return ESP_OK;
}

static void mqttModule_cmd_subscribe(esp_mqtt_event_handle_t event) {

	esp_mqtt_client_handle_t client = event->client;

	for(int i = 0; i < mqttCommand_count; i++) {
		esp_mqtt_client_subscribe(client, mqtt_command_list[i].topic, 0);
	}
}

__attribute__((weak)) void* mqttModule_cmd_parser(char *data, int data_len) {
	return NULL;
}

static void mqttModule_cmd_handler(esp_mqtt_event_handle_t event) {

	for(int i = 0; i < mqttCommand_count; i++) {

		if( !strncmp(event->topic, mqtt_command_list[i].topic, strlen(mqtt_command_list[i].topic)) ) {

			if(mqtt_command_list[i].data == NULL) {

				void *arg = mqttModule_cmd_parser(event->data, event->data_len);
				mqtt_command_list[i].call_back(arg);

			} else if(!strncmp(event->data, mqtt_command_list[i].data,strlen(mqtt_command_list[i].data)) ) {
					if(strlen(mqtt_command_list[i].data) == event->data_len) {
						mqtt_command_list[i].call_back(NULL);
					}
			}
		}
	}
}
