#ifndef mqttModule_H
#define mqttModule_H

#include "mqtt_client.h"

typedef struct {
	char* topic;
	char* data;
	void (*call_back)(void *args);
} mqtt_command_t;

void mqttCommand_Register(mqtt_command_t mqtt_command);
esp_err_t mqttModule_Start(esp_mqtt_client_config_t mqtt_config);
esp_err_t mqttModule_Stop();

__attribute__((weak)) void* mqttModule_cmd_parser(char *data, int data_len);

void MQTT_Module_Comands_Register();

#endif//mqttModule_H
