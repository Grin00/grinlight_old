#include "mqttModule.h"
#include "WS2812_Control.h"

#define LOG_LOCAL_LEVEL ESP_LOG_DEBUG
#include "esp_log.h"

static const char* TAG = "[mqttModule_dedicated]";

void *mqttModule_cmd_parser(char *data, int data_len)
{
    static WS2812_Color_t Color;
    int R, G, B;
    data[data_len] = '\0';// made end of the string
    if( sscanf(data, "%d %d %d", &R, &G, &B) != 3) // sscanf shoud fill 3 fields
        return NULL;

    if(R > 255 || G > 255 || B > 255)
        return NULL;
    if(R < 0 || G < 0 || B < 0)
        return NULL;

    Color.Red = (uint8_t)R;
    Color.Green = (uint8_t)G;
    Color.Blue = (uint8_t)B;
    
    ESP_LOGD(TAG, "%d %d %d", R, G, B);
    return (void *)(&Color);
}

void MQTT_Module_Comands_Register()
{
    mqtt_command_t power_on = {
        .topic = "grinlight/power/set",
        .data = "ON",
        .call_back = WS2812_PowerOn};

    mqtt_command_t power_off = {
        .topic = "grinlight/power/set",
        .data = "OFF",
        .call_back = WS2812_PowerOff};

    mqtt_command_t set_color = {
        .topic = "grinlight/color/set",
        .data = NULL,
        .call_back = WS2812_SetColor
    };

    mqttCommand_Register(power_on);
    mqttCommand_Register(power_off);
    mqttCommand_Register(set_color);
}