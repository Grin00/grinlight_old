#include "GrinLight.h"

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "WifiManager.h"
#include "mqttModule.h"
#include "WS2812_Control.h"

#define LOG_LOCAL_LEVEL ESP_LOG_INFO
#include "esp_log.h"

static const char *TAG = "MAIN";

static void Wifi_Module_Start();

void app_main()
{
    esp_err_t err;
    bool MQTT_is_init = false;
    system_event_id_t Wifi_State;
    esp_mqtt_client_config_t mqtt_config = {
        .uri = MQTT_URI,
        .port = MQTT_PORT,
        .username = MQTT_USERNAME,
        .password = MQTT_PASSWORD,
        .task_prio = MQTT_TASK_PRIO};

    WS2812_init();
    Wifi_Module_Start();
    MQTT_Module_Comands_Register();

    while (1)
    {
        Wifi_State = WifiManager_Get_state();
        if (Wifi_State == SYSTEM_EVENT_STA_GOT_IP && !MQTT_is_init)
        {
            err = mqttModule_Start(mqtt_config);
            ESP_ERROR_CHECK(err);

            MQTT_is_init = true;
        }
        vTaskDelay(MAIN_CYCLE_DELAY_MS / portTICK_PERIOD_MS);
    }
}

static void Wifi_Module_Start()
{

    esp_err_t err;

    WifiManager_config_t WifiManager_config = {
        .ap_ssid = WIFI_AP_SSID,
        .wifi_retry_conn_period_ms = WIFI_RECONN_PERIOD_MS};

    err = WifiManager_init(WifiManager_config);
    if (err != ESP_OK)
    {
        wifi_config_t wifi_def_conf = {
            .sta = {
                .ssid = DEFAULT_STA_SSID,
                .password = DEFAULT_STA_PASSWORD}};
        err = WifiManager_Set_STA_conf(wifi_def_conf);
        if (err == ESP_ERR_INVALID_ARG)
            ESP_LOGE(TAG, "DEFAULT_STA_SSID or DEFAULT_STA_PASSWORD is empty");
    }
}