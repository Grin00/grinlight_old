#
# "main" pseudo-component makefile.
#
# (Uses default behaviour of compiling all source files in directory, adding 'include' to include path.)

COMPONENT_TOTAL_DIRS += ./
COMPONENT_TOTAL_DIRS += ./WS2812_Control
COMPONENT_TOTAL_DIRS += ./MQTT_Module

COMPONENT_SRCDIRS += $(COMPONENT_TOTAL_DIRS)
COMPONENT_ADD_INCLUDEDIRS += $(COMPONENT_TOTAL_DIRS)